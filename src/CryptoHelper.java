import java.util.Scanner;

public class CryptoHelper {
    public static void main (String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the cryptogram. ");
        String cryptoText = keyboard.nextLine();
        cryptoText = cryptoText.toUpperCase();

        char[] cryptoArray = cryptoText.toCharArray();
        int length = cryptoText.length();
        char[] solutionArray = new char[length];

        for (int n = 0; n < length; n++) {
            char c = cryptoArray[n];
            if (!Character.isLetter(c))
                solutionArray[n] = c;
            else
                solutionArray[n] = ' ';
        }

        String solutionText = String.valueOf(solutionArray);
        String originalSolutionText = solutionText;
        String previousSolutionText = "";

        while (true) {
            System.out.println();
            System.out.println("Solution: " + solutionText);
            System.out.println("Crypto:   " + cryptoText);
            System.out.print("What do you want to do? "
                  + " C = Continue   Q = Quit  U = Undo last step   S = Start over >>> ");
            String input = keyboard.nextLine();
            char next = input.toUpperCase().charAt(0);

            while (next!= 'C' && next != 'Q' && next != 'S' && next != 'U') {
                System.out.print("What do you want to do? "
                  + " C = Continue   Q = Quit  U = Undo last step   S = Start over >>> ");
                input = keyboard.nextLine();
                next = input.toUpperCase().charAt(0);
            }

            if (next == 'Q') {
                break;
            }

            if (next == 'S') {
                previousSolutionText = solutionText;
                solutionText = originalSolutionText;
                solutionArray = solutionText.toCharArray();
                continue;
            }

            if (next == 'U') {
                if (previousSolutionText.length() == 0) {
                    System.out.println("Sorry. This program can " +
                               "only undo one previous step.");
                } else   {
                    solutionText = previousSolutionText;
                    solutionArray = solutionText.toCharArray();
                    previousSolutionText = ""; //Not sure if we need this line
                }
            }

            System.out.print("What letter in the cryptogram do you want to decode? ");
            input = keyboard.nextLine();
            char oldChar = input.toUpperCase().charAt(0);

            System.out.print("What do you want (" + oldChar + ") to be decoded as? ");
            input = keyboard.nextLine();
            char newChar = input.toUpperCase().charAt(0);

            for (int n = 0; n < length; n++) {
                if (solutionArray[n] == newChar) {
                    solutionArray[n] = ' ';
                } else if (oldChar == cryptoArray[n]) {
                    solutionArray[n] = newChar;
                }
            }
            previousSolutionText = solutionText;
            solutionText = String.valueOf(solutionArray);
        }
        System.out.println("Program is ending at your request.");
        keyboard.close();
    }
}